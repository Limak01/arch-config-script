# Ask for root password
echo 'Root password is needed for some commands executed by this script.'
if [[ ! $(sudo echo 0) ]]; then exit; fi

sudo pacman -S --needed - < pkglist.txt

echo 'Configuring packages...'
git clone 'https://gitlab.com/Limak01/dotfiles.git'
cp -R ./dotfiles/.xmonad $HOME/
cp -R ./dotfiles/.config $HOME/
cp -R ./dotfiles/.local $HOME/
cp ./dotfiles/.vimrc $HOME/
cp ./dotfiles/.vimrc.plug $HOME/
cp ./dotfiles/.zshrc $HOME/

# Get all needed fonts 
echo 'Configuring fonts...'
mkdir -p $HOME/.local/share/fonts/{ttf,otf}
cp -R ./fonts/Feather $HOME/.local/share/fonts/ttf/ 
wget 'https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/Meslo.zip'
unzip Meslo.zip -d $HOME/.local/share/fonts/ttf/Meslo/
wget 'https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/Mononoki.zip'
unzip Mononoki.zip -d $HOME/.local/share/fonts/ttf/Mononoki/
wget 'https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/RobotoMono.zip'
unzip RobotoMono.zip -d $HOME/.local/share/fonts/ttf/RobotoMono/
chown $USER:$USER $HOME/.local/share/fonts/*

#Copy ArcDarkest theme
mkdir $HOME/.themes
unzip ArcDarkest.zip -d $HOME/.themes/

# Terminal config
echo 'Configuring terminal...'
git clone 'https://github.com/zsh-users/zsh-autosuggestions' ~/.zsh/zsh-autosuggestions
git clone 'https://github.com/zsh-users/zsh-syntax-highlighting.git' ~/.zsh/zsh-syntax-highlighting
sudo chsh $USER -s "/bin/zsh"

# Neovim config
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim

#Language servers
sudo npm install -g typescript typescript-language-server vscode-langservers-extracted

echo 'Configuring login manager...'
git clone 'https://github.com/MarianArlt/sddm-chili.git' 
sudo mv ./sddm-chili /usr/share/sddm/themes/chili
sudo cp background.png /usr/share/sddm/themes/chili/assets/
cp .face.icon $HOME/

#Give sddm permission to read .face.icon file
setfacl -m u:sddm:x ~/
setfacl -m u:sddm:r ~/.face.icon

if [ -f "/usr/share/sddm/themes/chili/theme.conf" ]; then
    sudo cp /usr/share/sddm/themes/chili/theme.conf /usr/share/sddm/themes/chili/theme.conf.backup && \
    sudo sed -i 's/^background=assets\/background.jpg*.*/background=assets\/background.png/g' /usr/share/sddm/themes/chili/theme.conf
fi

if [ -f "/usr/lib/sddm/sddm.conf.d/default.conf" ]; then
    sudo cp /usr/lib/sddm/sddm.conf.d/default.conf /usr/lib/sddm/sddm.conf.d/default.conf.backup && \
    sudo sed -i 's/^Current=*.*/Current=chili/g' /usr/lib/sddm/sddm.conf.d/default.conf && \
    sudo mv /usr/lib/sddm/sddm.conf.d/default.conf /usr/lib/sddm/sddm.conf.d/sddm.conf
fi

sudo systemctl enable sddm.service

while true; do
    read -p "Installation is finished, do you want to reboot? [Y/n] " yn
    case $yn in
        [Yy]* ) reboot;;
        [Nn]* ) break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no.";;
    esac
done

